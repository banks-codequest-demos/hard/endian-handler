import sys
import math
import string

cases: int = int(sys.stdin.readline().rstrip())

def convert_to_big(little_endian: str) -> str:
    byte_size: int = 2
    byte_list: list[str] = [little_endian[i:i+byte_size] for i in range(0,
        len(little_endian), byte_size)]
    byte_list.reverse()
    big_endian: str = ''.join(byte_list)
    return big_endian

def standardize_endian(test_case: str) -> str:
    num, endian = test_case.split(' ')
    if endian == "LITTLE":
        num = convert_to_big(num)
    return num

def hex_to_decimal(hex_num: str) -> str:
    return str(int(hex_num, 16))

def handle_endian(test_case: str) -> str:
    print(hex_to_decimal(standardize_endian(test_case)))

for caseNum in range(cases):
    handle_endian(sys.stdin.readline().rstrip()) 
